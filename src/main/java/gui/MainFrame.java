package gui;

import gui.controller.ControllerFactory;
import gui.controller.MainController;
import gui.view.MainView;
import gui.view.ViewFactory;
import util.Logger;

import javax.swing.*;
import java.awt.*;

public class MainFrame extends JFrame {
    private MainView mainView;
    private MainController mainController;
    private CardLayout cardLayout;
    private Container container;
    private Logger logger;
    public static MainFrame mainFrame;

    public MainFrame() {
        logger = Logger.getInstance();
        logger.log(getClass().getSimpleName(), "Start Frame");
        cardLayout = new CardLayout();
        container = getContentPane();
        initializeFrame();
        mainFrame = this;
    }

    public void initializeFrame() {
        logger.log(getClass().getSimpleName(), "initializeFrame()");
        setSize(600, 500);
        setMinimumSize(new Dimension(500, 400));
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainView = (MainView) ViewFactory.getView(ViewFactory.MAIN);
        mainView.setCardLayout(cardLayout);
        mainView.setContainer(container);
        mainView.setLayoutToContainer();
        mainController = (MainController) ControllerFactory.getController(mainView);
        add(mainView);
        mainController.startController();
        setVisible(true);
        pack();
    }
}
