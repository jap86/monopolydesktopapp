package gui.component;

import util.Logger;

import javax.swing.*;

public class ColorComboBox extends JComboBox {
    private ComboBoxColor previousColor;
    private ComboBoxColor selectedColor;
    private Logger logger = Logger.getInstance();
    private ComboBoxModel comboBoxModel;
    private boolean isFirstSelection = true;

    public ColorComboBox() {
        this.comboBoxModel = new ComboBoxModel();
        setModel(comboBoxModel);
        setRenderer(new ColorBoxCellRenderer());
        setName(getClass().getSimpleName());
    }

    public String getColorName() {
        ComboBoxColor comboBoxColor = (ComboBoxColor) getSelectedItem();
        return comboBoxColor.name();
    }

    public void removeColor(ComboBoxColor comboBoxColor) {
        comboBoxModel.removeElement(comboBoxColor);
        log("Removed color: " + comboBoxColor.getColorName());
    }

    public void addColor(ComboBoxColor comboBoxColor) {
        comboBoxModel.addElement(comboBoxColor);
        log("Added color: " + comboBoxColor.getColorName());
    }

    @Override
    public void setSelectedItem(Object anObject) {
        previousColor = (ComboBoxColor) getSelectedItem();
        selectedColor = (ComboBoxColor) anObject;
        comboBoxModel.setSelectedItem(selectedColor);
        log("Select color " + selectedColor.getColorName() + ", previous selected color was: " + previousColor.getColorName());
        if (previousColor == selectedColor) {
            return;
        }
        if (isFirstSelection) {
            isFirstSelection = false;
            return;
        }
        fireActionEvent();
    }

    public ComboBoxColor getColor() {
        return (ComboBoxColor) getSelectedItem();
    }

    public ComboBoxColor getPreviousColor() {
        return previousColor;
    }

    private void log(String message) {
        logger.log(getName(), message);
    }

    public void fillRowNr(Integer rowNr) {
        setName(getClass().getSimpleName() + " " + rowNr);
    }
}
