package gui.component;

import java.awt.event.ActionEvent;
import java.util.ArrayList;

public class ColorComboBoxList extends ArrayList<ColorComboBox> {

    public ColorComboBoxList(int size) {
        super(size);
        for (int i = 0; i < size; i++) {
            ColorComboBox colorComboBox = new ColorComboBox();
            colorComboBox.addActionListener(event -> colorSelected(event));
            super.add(colorComboBox);
        }
    }

    @Override
    public boolean add(ColorComboBox colorComboBox) {
        return false;
    }

    public void colorSelected(ActionEvent event) {
        Object source = event.getSource();
        if (source instanceof ColorComboBox) {
            ColorComboBox colorComboBox = (ColorComboBox) source;
            int index = indexOf(colorComboBox);
            removeColorFromOthers(colorComboBox.getColor(), index);
            addColorToOthers(colorComboBox.getPreviousColor(), index);
        }
    }

    private void removeColorFromOthers(ComboBoxColor colorToBeRemoved, int indexBoxToBeSkipped) {
        stream()
                .filter(colorComboBox -> indexOf(colorComboBox) != indexBoxToBeSkipped)
                .forEach(colorComboBox -> colorComboBox.removeColor(colorToBeRemoved));
    }

    private void addColorToOthers(ComboBoxColor colorToBeAdded, int indexBoxToBeSkipped) {
        stream()
                .filter(colorComboBox -> indexOf(colorComboBox) != indexBoxToBeSkipped)
                .forEach(colorComboBox -> colorComboBox.addColor(colorToBeAdded));
    }
}
