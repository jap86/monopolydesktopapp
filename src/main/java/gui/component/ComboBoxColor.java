package gui.component;

import java.awt.*;

public enum ComboBoxColor {
    CHOOSE_COLOR(null, "colorComboBox.chooseColor"),
    DARK_RED(new Color(139, 0, 0),"colorComboBox.darkRed"),
    RED(new Color(250, 0, 0), "colorComboBox.red"),
    ORANGE(new Color(255, 165, 0), "colorComboBox.orange"),
    YELLOW(new Color(255, 255, 0), "colorComboBox.yellow"),
    TURQUOISE(new Color(64, 224, 208), "colorComboBox.turquoise"),
    GREEN(new Color(0, 128, 0), "colorComboBox.green"),
    LIME(new Color(0, 255, 0), "colorComboBox.lime"),
    CYAN(new Color(0, 255, 255), "colorComboBox.cyan"),
    BLUE(new Color(0, 0, 255), "colorComboBox.blue"),
    NAVY(new Color(0, 0, 128), "colorComboBox.navy"),
    PURPLE(new Color(128, 0, 128), "colorComboBox.purple"),
    PINK(new Color(255, 0, 255), "colorComboBox.pink"),
    AQUA_MARINE(new Color(127, 255, 212), "colorComboBox.aquaMarine"),
    LAVENDER(new Color(220, 208, 255), "colorComboBox.lavender"),
    SIENNA(new Color(160, 82, 45), "colorComboBox.sienna"),
    WHITE(new Color(250, 250, 250), "colorComboBox.white"),
    BLACK(new Color(0, 0, 0), "colorComboBox.black");

    private Color color;
    private String colorName;
    private String messageResource;

    private ComboBoxColor(Color color, String messageResource) {
        this.color = color;
        fillColorName();
        this.messageResource = messageResource;
    }

    //
    private void fillColorName() {
        StringBuilder stringBuilder = new StringBuilder();
        String unformattedColorName = name().toLowerCase();
        boolean shouldBeUpperCase = true;
        for (int i = 0; i < unformattedColorName.length(); i++) {
            String subString = unformattedColorName.substring(i, i + 1);
            if (shouldBeUpperCase) {
                subString = subString.toUpperCase();
                shouldBeUpperCase = false;
            }
            if (subString.equals("_")) {
                subString = " ";
                shouldBeUpperCase = true;
            }
            stringBuilder.append(subString);
        }
        colorName = stringBuilder.toString();
    }

    public boolean isColor() {
        return color != null;
    }

    public Color getColor() {
        return color;
    }

    public String getColorName() {
        return colorName;
    }

    @Override
    public String toString() {
        if (this == null) {
            return "null";
        }
        return colorName;
    }

    public String getMessageResource() {
        return messageResource;
    }
}
