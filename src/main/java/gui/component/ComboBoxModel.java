package gui.component;

import javax.swing.*;
import java.util.*;

public class ComboBoxModel extends AbstractListModel<gui.component.ComboBoxColor> implements MutableComboBoxModel<ComboBoxColor> {
    private ComboBoxColor[] comboBoxColorArray;
    private List<ComboBoxColor> comboBoxColorList;
    private Map<ComboBoxColor, Integer> indexesColors;
    private List<ComboBoxColor> deletedColors;
    private Object selectedItem;

    public ComboBoxModel() {
        comboBoxColorArray = ComboBoxColor.values();
        comboBoxColorList = new ArrayList<>(Arrays.asList(comboBoxColorArray));
        indexesColors = new HashMap<>();
        deletedColors = new ArrayList<>();
        fillIndexes();
        selectedItem = comboBoxColorList.get(0);
    }

    private void fillIndexes() {
        for (int i = 0; i < comboBoxColorArray.length; i++) {
            indexesColors.put(comboBoxColorArray[i], i);
        }
    }

    @Override
    public int getSize() {
        updateComboBoxColorsList();
        return comboBoxColorList.size();
    }

    @Override
    public gui.component.ComboBoxColor getElementAt(int index) {
        updateComboBoxColorsList();
        if (index >= 0 && index < comboBoxColorList.size()) {
            return comboBoxColorList.get(index);
        }
        return null;
    }

    @Override
    public void addElement(ComboBoxColor comboBoxColor) {
        Integer index = getIndex(comboBoxColor);
        comboBoxColorArray[index] = comboBoxColor;
        updateComboBoxColorsList();
    }

    @Override
    public void removeElement(Object obj) {
        if (obj instanceof ComboBoxColor) {
            ComboBoxColor comboBoxColor = (ComboBoxColor) obj;
            removeElementAt(getIndex(comboBoxColor));
        }
    }

    @Override
    public void insertElementAt(ComboBoxColor comboBoxColor, int index) {
        comboBoxColorArray[index] = comboBoxColor;
        updateComboBoxColorsList();
    }

    @Override
    public void removeElementAt(int index) {
        comboBoxColorArray[index] = null;
        updateComboBoxColorsList();
    }

    @Override
    public void setSelectedItem(Object item) {
        if (item != null) {
            selectedItem = item;
            fireContentsChanged(this, -1, -1);
        }
    }

    @Override
    public Object getSelectedItem() {
        return selectedItem;
    }

    private Integer getIndex(ComboBoxColor comboBoxColor) {
        Integer index = indexesColors.get(comboBoxColor);
        if (index == null) {
            throw new RuntimeException("Color not in available.");
        }
        return index;
    }

    private void updateComboBoxColorsList() {
        comboBoxColorList.clear();
        for (int i = 0; i < comboBoxColorArray.length; i++) {
            ComboBoxColor comboBoxColor = comboBoxColorArray[i];
            if (comboBoxColor == null) {
                continue;
            }
            comboBoxColorList.add(comboBoxColor);
        }
    }
}
