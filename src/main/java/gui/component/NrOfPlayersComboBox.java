package gui.component;

import javax.swing.*;

public class NrOfPlayersComboBox extends JComboBox {
    public NrOfPlayersComboBox() {
        DefaultComboBoxModel defaultComboBoxModel = new DefaultComboBoxModel();
        defaultComboBoxModel.addElement(NumberOfPlayers.TWO_PLAYERS);
        defaultComboBoxModel.addElement(NumberOfPlayers.THREE_PLAYERS);
        defaultComboBoxModel.addElement(NumberOfPlayers.FOUR_PLAYERS);
        defaultComboBoxModel.addElement(NumberOfPlayers.FIVE_PLAYERS);
        defaultComboBoxModel.addElement(NumberOfPlayers.SIX_PLAYERS);
        setModel(defaultComboBoxModel);
        setRenderer(new NrOfPlayersComboBoxCellRenderer());
        setName(getClass().getSimpleName());
    }

    public Integer getNrOfPlayersSelected() {
        NumberOfPlayers selectedItem = (NumberOfPlayers) getSelectedItem();
        return selectedItem.getNrOfPlayers();
    }

    public void setNrOfPlayersSelected(Integer nrOfPlayersSelected) {
        switch (nrOfPlayersSelected) {
            case 3: setSelectedItem(NumberOfPlayers.THREE_PLAYERS);
            break;
            case 4: setSelectedItem(NumberOfPlayers.FOUR_PLAYERS);
            break;
            case 5: setSelectedItem(NumberOfPlayers.FIVE_PLAYERS);
            break;
            case 6: setSelectedItem(NumberOfPlayers.SIX_PLAYERS);
            break;
            default: setSelectedItem(NumberOfPlayers.TWO_PLAYERS);
        }
    }
}
