package gui.component;

import messages.Messages;

import javax.swing.*;
import java.awt.*;

public class NrOfPlayersComboBoxCellRenderer implements ListCellRenderer {

    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        JLabel jLabel = new JLabel();
        if (value instanceof NumberOfPlayers) {
            NumberOfPlayers numberOfPlayers = (NumberOfPlayers) value;
            jLabel.setText(localizedValueNumberOfPlayers(numberOfPlayers));
        }

        return jLabel;
    }

    private String localizedValueNumberOfPlayers(NumberOfPlayers nrOfPlayers) {
        return Messages.getMessage("nrOfPlayers.nrOfPlayers", nrOfPlayers.toString());
    }
}
