package gui.component;

public enum NumberOfPlayers {
    TWO_PLAYERS(2),
    THREE_PLAYERS(3),
    FOUR_PLAYERS(4),
    FIVE_PLAYERS(5),
    SIX_PLAYERS(6);

    private Integer nrOfPlayers;

    NumberOfPlayers(Integer nrOfPlayers) {
        this.nrOfPlayers = nrOfPlayers;
    }

    @Override
    public String toString() {
        return nrOfPlayers.toString();
    }

    public Integer getNrOfPlayers() {
        return nrOfPlayers;
    }
}
