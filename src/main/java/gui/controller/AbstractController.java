package gui.controller;

import util.Logger;

public abstract class AbstractController implements Controller {
    private Logger logger;

    public AbstractController() {
        logger = Logger.getInstance();
    }

    protected void log(String message) {
        logger.log(getClass().getSimpleName(), message);
    }

    protected void log(String className, String message) {
        logger.log(className, message);
    }
}
