package gui.controller;

import gui.listeners.MainMenuListener;
import gui.view.MainMenuView;

public class MainMenuController extends AbstractController {

    private MainMenuView mainMenuView;
    private MainMenuListener mainMenuListener;

    public MainMenuController(MainMenuView mainMenuView) {
        this.mainMenuView = mainMenuView;

        addActionListenerToNewGameButton();
        addActionListenerToLoadGameButton();
        addActionListenerToChooseLanguageButton();
    }

    private void addActionListenerToNewGameButton() {
        mainMenuView.setActionListenerNewGameButton(
                event -> mainMenuListener.newGameButtonPressed());
    }

    private void addActionListenerToLoadGameButton() {
        mainMenuView.setActionListenerToLoadGameButton(
                event -> mainMenuListener.loadGameButtonPressed());
    }

    private void addActionListenerToChooseLanguageButton() {
        mainMenuView.setActionListenerToLanguageButton(
                event -> mainMenuListener.languageButtonPressed());
    }

    @Override
    public void startController() {
        mainMenuView.initializeView();
    }

    public void setMainMenuListener(MainMenuListener mainMenuListener) {
        this.mainMenuListener = mainMenuListener;
    }
}
