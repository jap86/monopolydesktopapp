package gui.controller;

import gui.component.ColorComboBoxList;
import gui.component.NrOfPlayersComboBox;
import gui.listeners.PlayerSetupRowListenerImpl;
import gui.listeners.PlayersSetupListener;
import gui.view.PlayerSetupRowView;
import gui.view.PlayersSetupView;
import gui.view.ViewFactory;
import settings.Settings;
import valueObjects.PlayerValueObject;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class PlayersSetupController extends AbstractController {
    private PlayersSetupView playersSetupView;
    private PlayersSetupListener playersSetupListener;
    private PlayersSetupRowControllerList playersSetupRowControllerList;
    private ColorComboBoxList colorComboBoxes = new ColorComboBoxList(Settings.MAX_NR_PLAYERS);
    private HashMap<Integer, String> playerNameMap;
    private int nrOfPlayers;

    public PlayersSetupController(PlayersSetupView playersSetupView) {
        this.playersSetupView = playersSetupView;
        playerNameMap = new HashMap<>();
        nrOfPlayers = Settings.DEFAULT_NR_OF_PLAYERS;
        playersSetupView.setNrOfPlayersSelectedInComboBox(nrOfPlayers);
        PlayerSetupRowView[] playerSetupRowViews = createPlayersSetupRowViews();
        createPlayersSetupRowControllers(playerSetupRowViews);
        playersSetupView.setPlayerSetupRowViews(playerSetupRowViews);
        fillPlayerNameMap();
        playersSetupView.setEnableStartGameButton(false);
    }

    private void fillPlayerNameMap() {
        for (int i = 1; i <= Settings.MAX_NR_PLAYERS; i++) {
            playerNameMap.put(i, " ");
        }
    }

    private PlayerSetupRowView[] createPlayersSetupRowViews() {
        PlayerSetupRowView [] playerSetupRowViews = new PlayerSetupRowView[Settings.MAX_NR_PLAYERS];
        for (int i = 0; i < playerSetupRowViews.length; i++) {
            PlayerSetupRowView playerSetupRowView = (PlayerSetupRowView) ViewFactory.getView(ViewFactory.SETUP_PLAYERS_ROW);
            playerSetupRowViews[i] = playerSetupRowView;
            playerSetupRowView.fillColorComboBox(colorComboBoxes.get(i));
        }
        return playerSetupRowViews;
    }

    private void createPlayersSetupRowControllers(PlayerSetupRowView[] playerSetupRowViews) {
        playersSetupRowControllerList = new PlayersSetupRowControllerList();
        for (int i= 0; i < playerSetupRowViews.length; i++) {
            PlayerSetupRowView playerSetupRowView = playerSetupRowViews[i];
            PlayersSetupRowController playersSetupRowController = (PlayersSetupRowController) ControllerFactory.getController(playerSetupRowView);
            playersSetupRowController.setPlayerSetupRowListener(new PlayerSetupRowListenerImpl(playersSetupRowController, this));
            playersSetupRowController.fillRowNr(i + 1);
            playersSetupRowControllerList.add(playersSetupRowController);
        }
    }

    @Override
    public void startController() {
        log("startController()");
        playersSetupRowControllerList.startPlayersSetupRowControllers();
        playersSetupRowControllerList.showRows(Settings.DEFAULT_NR_OF_PLAYERS);
        playersSetupView.initializeView();
        addActionListeners();
    }

    private void addActionListeners() {
        addActionListenerNrOfPlayerComboBox();
        addActionListenerStartGameButton();
        addActionListenerGoToMainMenuButton();
    }

    private void addActionListenerNrOfPlayerComboBox() {
        playersSetupView.addActionListenerNrOfPlayersComboBox(
                event -> {
                    NrOfPlayersComboBox nrOfPlayersComboBox = (NrOfPlayersComboBox) event.getSource();
                    nrOfPlayers = nrOfPlayersComboBox.getNrOfPlayersSelected();
                    playersSetupView.setNrOfPlayersSelectedInComboBox(nrOfPlayers);
                    playersSetupRowControllerList.showRows(nrOfPlayers);
                }
            );
    }

    private void addActionListenerStartGameButton() {
        playersSetupView.addActionListenerStartGameButton(
                event -> playersSetupListener.startGameButtonPressed()
        );
    }

    private void addActionListenerGoToMainMenuButton() {
        playersSetupView.addActionListenerGoToMainMenuButton(
                event -> playersSetupListener.goToMainMenuButtonPressed()
        );
    }

    public void nameInserted(String name, int rowNr) {
        if (name == null) {
            return;
        }
        playerNameMap.put(rowNr, name);
        playersSetupView.setEnableStartGameButton(determineStartButtonShouldBeEnabled());
    }

    private boolean determineStartButtonShouldBeEnabled() {
        Set<String> playerNames = new HashSet<>();
        for (int i = 1; i <= nrOfPlayers; i++) {
            String name = playerNameMap.get(i);
            if (name.isBlank() || !playerNames.add(name)) {
                return false;
            }
        }
        return true;
    }

    public List<PlayerValueObject> getPlayerValueObjects() {
        return playersSetupRowControllerList.getPlayerValueObjects();
    }

    public void setPlayersSetupListener(PlayersSetupListener playersSetupListener) {
        this.playersSetupListener = playersSetupListener;
    }

    public void refresh() {
        playersSetupRowControllerList.clear();
    }

    public PlayersSetupRowControllerList getPlayersSetupRowControllerList() {
        return playersSetupRowControllerList;
    }
}
