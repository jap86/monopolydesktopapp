package gui.controller;

import gui.component.ColorComboBox;
import gui.listeners.PlayerSetupRowListener;
import gui.view.PlayerSetupRowView;
import valueObjects.PlayerValueObject;

public class PlayersSetupRowController extends AbstractController {
    private PlayerSetupRowView playerSetupRowView;
    private PlayerSetupRowListener playerSetupRowListener;
    private PlayerValueObject playerValueObject;
    private boolean isVisible;
    private int rowNr;

    public PlayersSetupRowController(PlayerSetupRowView playerSetupRowView) {
        this.playerSetupRowView = playerSetupRowView;
        playerValueObject = new PlayerValueObject();
    }

    @Override
    public void startController() {
        playerSetupRowView.initializeView();
        addActionListeners();
    }

    private void addActionListeners() {
        addActionListenerToPlayerNameTextField();
        addActionListenerToIsComputerCheckBox();
        addActionListenerToColorComboBox();
    }

    private void addActionListenerToPlayerNameTextField() {
        playerSetupRowView.addActionListenerToPlayerName(
                event -> playerSetupRowListener.nameInserted(event)
        );
    }

    private void addActionListenerToIsComputerCheckBox() {
        playerSetupRowView.addActionListenerToIsComputerCheckBox(
                event -> {
                    playerSetupRowListener.computerBoxTicked();
                }
        );
    }

    private void addActionListenerToColorComboBox() {
        playerSetupRowView.addActionListenerToColorComboBox(
                event -> {
                    ColorComboBox colorComboBox = (ColorComboBox)  event.getSource();
                    playerSetupRowListener.colorSelected(colorComboBox.getColorName());
                }
        );
    }

    public PlayerValueObject getPlayerValueObject() {
        return playerValueObject;
    }

    public String getSelectedColor() {
        return playerSetupRowView.getPlayerColor();
    }

    public void determineVisibility(int nrOfRows) {
        isVisible = rowNr <= nrOfRows;
        if (isVisible) {
            showRow();
        } else {
            hideRow();
        }
    }

    private void showRow() {
        log("Show row " + rowNr);
        playerSetupRowView.setVisibilityComponents(true);
    }

    public void hideRow() {
        log("Hide row " + rowNr);
        playerSetupRowView.setVisibilityComponents(false);
    }

    public boolean getIsVisible() {
        return isVisible;
    }

    public void fillRowNr(int rowNr) {
        playerSetupRowView.fillRowNr(rowNr);
        this.rowNr = rowNr;
    }

    public int getRowNr() {
        return playerSetupRowView.getRowNr();
    }

    public void setPlayerSetupRowListener(PlayerSetupRowListener playerSetupRowListener) {
        this.playerSetupRowListener = playerSetupRowListener;
    }

    @Override
    protected void log(String message) {
        super.log(this.getClass().getSimpleName() + rowNr, message);
    }
}
