package gui.controller;

import valueObjects.PlayerValueObject;

import java.util.ArrayList;
import java.util.List;

public class PlayersSetupRowControllerList extends ArrayList<PlayersSetupRowController> {

    public void startPlayersSetupRowControllers() {
        forEach(row -> row.startController());
    }

    public void showRows(int nrOfRows) {
        forEach(row -> row.determineVisibility(nrOfRows));
    }

    public List<PlayerValueObject> getPlayerValueObjects() {
        List<PlayerValueObject> playerValueObjects = new ArrayList<>();
        this.stream().filter(row -> row.getIsVisible()).forEach(row -> {
            playerValueObjects.add(row.getPlayerValueObject());
        });
        return playerValueObjects;
    }
}
