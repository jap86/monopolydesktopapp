package gui.listeners;

import util.Logger;

public class Listener {
    private Logger logger;

    public Listener() {
        logger = Logger.getInstance();
    }

    protected void log(String message) {
        logger.log(getClass().getSimpleName(), message);
    }

    protected void logError(String message) {
        logger.logError(getClass().getSimpleName(), message);
    }
}
