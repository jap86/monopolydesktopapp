package gui.listeners;

import gui.controller.MainController;
import gui.controller.MainMenuController;

public class MainMenuListenerImpl extends Listener implements MainMenuListener {
    private MainController mainController;
    private MainMenuController mainMenuController;

    public MainMenuListenerImpl(MainController mainController, MainMenuController mainMenuController) {
        this.mainController = mainController;
        this.mainMenuController = mainMenuController;
    }

    @Override
    public void newGameButtonPressed() {
        mainController.showSelectNumberOfPlayersView();
    }

    @Override
    public void loadGameButtonPressed() {
        mainController.showLoadGameDialog();
    }

    @Override
    public void languageButtonPressed() {
        mainController.showChooseLanguageView();
    }
}
