package gui.listeners;

import java.awt.event.ActionEvent;

public interface PlayerSetupRowListener {
    void colorSelected(String color);
    void computerBoxTicked();
    void nameInserted(ActionEvent event);
}
