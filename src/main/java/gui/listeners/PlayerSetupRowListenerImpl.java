package gui.listeners;

import gui.component.PlayerNameTextField;
import gui.controller.PlayersSetupController;
import gui.controller.PlayersSetupRowController;
import valueObjects.PlayerValueObject;

import java.awt.event.ActionEvent;

public class PlayerSetupRowListenerImpl extends Listener implements PlayerSetupRowListener {
    private PlayersSetupRowController playersSetupRowController;
    private PlayersSetupController playersSetupController;
    private PlayerValueObject playerValueObject;
    public PlayerSetupRowListenerImpl(PlayersSetupRowController playersSetupRowController, PlayersSetupController playersSetupController) {
        this.playersSetupRowController = playersSetupRowController;
        this.playersSetupController = playersSetupController;
    }

    @Override
    public void colorSelected(String color) {
        playerValueObject = playersSetupRowController.getPlayerValueObject();
        playerValueObject.playerColor = color;
    }

    @Override
    public void computerBoxTicked() {

    }

    @Override
    public void nameInserted(ActionEvent event) {
        Object source = event.getSource();
        if (!(source instanceof PlayerNameTextField)) {
            return;
        }
        PlayerNameTextField playerNameTextField = (PlayerNameTextField) source;
        String name = playerNameTextField.getText();
        playersSetupController.nameInserted(name, playersSetupRowController.getRowNr());
    }
}
