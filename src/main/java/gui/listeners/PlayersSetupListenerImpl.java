package gui.listeners;

import exceptions.BadNameException;
import gui.controller.MainController;
import gui.controller.PlayersSetupController;
import services.PlayersSetupService;
import valueObjects.PlayerValueObject;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class PlayersSetupListenerImpl extends Listener implements PlayersSetupListener {
    private MainController mainController;
    private PlayersSetupController playersSetupController;
    private PlayersSetupService playersSetupService;
    private List<PlayerValueObject> playerValueObjects;

    public PlayersSetupListenerImpl(MainController mainController, PlayersSetupController playersSetupController) {
        this.mainController = mainController;
        this.playersSetupController = playersSetupController;
        playersSetupService = new PlayersSetupService();
    }

    @Override
    public void startGameButtonPressed() {
        playerValueObjects = playersSetupController.getPlayerValueObjects();
        if (!playerValueObjectsAreValid())
        for (PlayerValueObject playerValueObject : playerValueObjects) {
            addPlayer(playerValueObject);
        }
        mainController.startMonopolyGame(playerValueObjects);
    }

    private boolean playerValueObjectsAreValid() {
        Set<String> playerNames = new HashSet<>();
        for (PlayerValueObject playerValueObject : playerValueObjects) {
            String playerName = playerValueObject.name;
            if (playerName == null || !playerNames.add(playerName)) {
                return false;
            }
        }
        return true;
    }

    private void addPlayer(PlayerValueObject playerValueObject) {
        log("addPlayer() : Add " + playerValueObject);
        try {
            playersSetupService.addPlayer(playerValueObject);
        } catch (BadNameException badNameException) {
            String message = badNameException.getBadNameType().getMessage();
            throw new RuntimeException(message);
        }
    }

    @Override
    public void goToMainMenuButtonPressed() {
        playersSetupController.refresh();
        mainController.showMenu();
    }
}
