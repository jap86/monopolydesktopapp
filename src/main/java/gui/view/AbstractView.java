package gui.view;

import messages.Messages;
import util.Logger;

import javax.swing.*;

public abstract class AbstractView extends JPanel implements View {
    private static Messages messages;
    protected boolean isInitialized;
    private Logger logger;

    public AbstractView() {
        if (messages == null) {
            messages = new Messages();
        }

        logger = Logger.getInstance();
        initializeComponents();
        setVisible(false);
    }

    @Override
    public void initializeView() {
        log("initializeView()");

        if (isInitialized) {
            return;
        }
        layoutComponents();
        isInitialized = true;
    }

    protected void log(String message) {
        logger.log(getClass().getSimpleName(), message);
    }

    protected void log(String className, String message) {
        logger.log(className, message);
    }

    protected abstract void initializeComponents();

    protected abstract void layoutComponents();

    public String getMessage(String messageResource) {
        return messages.getMessage(messageResource);
    }

    public String getMessage(String messageResource, String ... args) {
        return messages.getMessage(messageResource, args);
    }
}
