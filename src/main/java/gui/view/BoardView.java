package gui.view;

import gui.component.BoardComponent;
import valueObjects.BoardSpaceValueObject;
import valueObjects.PlayerValueObject;

import java.awt.*;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

public class BoardView extends AbstractView {
    private Set<BoardComponent> boardComponents;

    @Override
    protected void initializeComponents() {
        log("initializeComponents()");
        setVisible(true);
        setLayout(new GridLayout(11,11));
        setBackground(Color.LIGHT_GRAY);
    }

    @Override
    protected void layoutComponents() {

    }

    public void fillBoardComponents(List<BoardSpaceValueObject> boardSpaceValueObjects, List<PlayerValueObject> playerValueObjects) {
        boardComponents = new HashSet<>();
        for (BoardSpaceValueObject boardSpaceValueObject : boardSpaceValueObjects) {
            BoardComponent boardComponent;

            if (boardSpaceValueObject == null) {
                boardComponent = new BoardComponent();
            } else {
                String boardComponentName = getMessage(boardSpaceValueObject.name);
                int boardComponentIdentifier = boardSpaceValueObject.identifier;
                boardComponent = new BoardComponent(boardComponentName, boardComponentIdentifier, playerValueObjects);
                boardComponents.add(boardComponent);
                log("Add " + boardComponentName + " to board.");
            }
            boardComponent.setVisible(true);
            add(boardComponent);
        }

    }

    public void setPlayerOnBoardComponent(String playerName, Integer boardComponentIdentifier) {
        Optional<BoardComponent> boardComponentOptional = boardComponents.stream()
                .filter(boardComp -> boardComp.getBoardComponentIdentifier().equals(boardComponentIdentifier))
                .findFirst();

        if (boardComponentOptional.isPresent()) {
            boardComponentOptional.get().putPlayerOnBoardSpace(playerName);
        }
    }

    public void removePlayerFromBoardComponent(String playerName, Integer boardComponentIdentifier) {
        Optional<BoardComponent> boardComponentOptional = boardComponents.stream()
                .filter(boardComp -> boardComp.getBoardComponentIdentifier().equals(boardComponentIdentifier))
                .findFirst();

        if (boardComponentOptional.isPresent()) {
            boardComponentOptional.get().removePlayerFromBoardSpace(playerName);
        }
    }

    public void emptyBoard() {
        boardComponents = null;
    }
}
