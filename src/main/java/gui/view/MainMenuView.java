package gui.view;

import gui.component.MenuButton;

import java.awt.event.ActionListener;

public class MainMenuView extends AbstractGridBagView {
    private MenuButton newGameButton;
    private MenuButton loadGameButton;
    private MenuButton languageButton;

    @Override
    protected void initializeComponents() {
        log("initializeComponents()");
        newGameButton = new MenuButton(getMessage("startMenu.newGame"));
        loadGameButton = new MenuButton(getMessage("startMenu.loadGame"));
        languageButton = new MenuButton(getMessage("startMenu.language"));
    }

    @Override
    protected void layoutComponents() {
        log("layOutComponents");
        addComponentToGridBagConstraints(newGameButton, 1, 1);
        addComponentToGridBagConstraints(loadGameButton, 1, 2);
        addComponentToGridBagConstraints(languageButton, 1, 3);
    }

    public void setActionListenerNewGameButton(ActionListener actionListener) {
        newGameButton.addActionListener(actionListener);
    }

    public void setActionListenerToLoadGameButton(ActionListener actionListener) {
        loadGameButton.addActionListener(actionListener);
    }

    public void setActionListenerToLanguageButton(ActionListener actionListener) {
        languageButton.addActionListener(actionListener);
    }
}
