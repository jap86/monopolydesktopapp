package gui.view;

import gui.component.ColorComboBox;
import gui.component.ComboBoxColor;
import gui.component.PlayerNameTextField;
import messages.Messages;
import util.Util;

import javax.swing.*;
import java.awt.event.ActionListener;

public class PlayerSetupRowView extends AbstractView {
    private JLabel playerLabel;
    private PlayerNameTextField playerName;
    private JCheckBox isComputer;
    private ColorComboBox colorComboBox;
    private Integer rowNr;

    @Override
    protected void initializeComponents() {
        log("initializeComponents()");
        playerLabel = new JLabel();
        playerName = new PlayerNameTextField(Messages.getMessage("selectNrOfPlayers.player", Util.stringValue(rowNr)));
        isComputer = new JCheckBox();
    }

    @Override
    protected void log(String message) {
        if (rowNr == null) {
            super.log(message);
        }
        super.log(getClass().getSimpleName() + " " + rowNr, message);
    }

    @Override
    protected void layoutComponents() {
        log("layOutComponents");
        add(playerLabel);
        add(playerName);
        add(isComputer);
        add(colorComboBox);
    }

    public void setVisibilityComponents(boolean showComponent) {
        playerLabel.setVisible(showComponent);
        playerName.setVisible(showComponent);
        playerName.setText(showComponent ? playerName.getText() : "");
        isComputer.setVisible(showComponent);
        isComputer.setSelected(showComponent ? isComputer.isSelected() : false);
        colorComboBox.setVisible(showComponent);
        colorComboBox.setSelectedItem(ComboBoxColor.CHOOSE_COLOR);
    }

    public void addActionListenerToPlayerName(ActionListener actionListener) {
        playerName.addActionListener(actionListener);
    }

    public void addActionListenerToIsComputerCheckBox(ActionListener actionListener) {
        isComputer.addActionListener(actionListener);
    }

    public void addActionListenerToColorComboBox(ActionListener actionListener) {
        colorComboBox.addActionListener(actionListener);
    }

    public JLabel getPlayerLabel() {
        return playerLabel;
    }

    public PlayerNameTextField getPlayerNameTextField() {
        return playerName;
    }

    public String getPlayerName() {return playerName.getText();}

    public JCheckBox getIsComputerCheckBox() {
        return isComputer;
    }

    public Boolean getIsComputer() {
        return isComputer.isSelected();
    }

    public String getPlayerColor() {
        return colorComboBox.getColorName();
    }

    public ColorComboBox getColorComboBox() {
        return colorComboBox;
    }

    public void fillColorComboBox(ColorComboBox colorComboBox) {
        this.colorComboBox = colorComboBox;
    }

    public void fillRowNr(Integer rowNr) {
        this.rowNr = rowNr;
        playerLabel.setText(Messages.getMessage("selectNrOfPlayers.playerName", Util.stringValue(rowNr)));
        colorComboBox.fillRowNr(rowNr);
    }

    public int getRowNr() {
        return rowNr;
    }
}
