package gui.view;

import gui.component.NrOfPlayersComboBox;

import javax.swing.*;
import java.awt.event.ActionListener;

public class PlayersSetupView extends AbstractGridBagView {
    private NrOfPlayersComboBox nrOfPlayersComboBox;
    private PlayerSetupRowView[] playerSetupRowViews;
    private JButton startGame;
    private JButton goToMainMenu;

    @Override
    protected void initializeComponents() {
        log("initializeComponents()");
        nrOfPlayersComboBox = new NrOfPlayersComboBox();
        startGame = new JButton(getMessage("selectNrOfPlayers.startGame"));
        goToMainMenu = new JButton(getMessage("selectNrOfPlayers.goToMainMenu"));
    }

    protected void layoutComponents() {
        log("layOutComponents");
        int xCoordinateFirstColumn = 1;
        int xCoordinateSecondColumn = 2;
        int yCoordinate = 1;
        addComponentToGridBagConstraints(nrOfPlayersComboBox, xCoordinateFirstColumn, yCoordinate++);

        for (PlayerSetupRowView playerSetupRowView : playerSetupRowViews) {
            addRowToGridBagConstraint(playerSetupRowView, yCoordinate++);
        }
        addComponentToGridBagConstraints(startGame, xCoordinateFirstColumn, yCoordinate);
        addComponentToGridBagConstraints(goToMainMenu, xCoordinateSecondColumn, yCoordinate);
    }

    private void addRowToGridBagConstraint(PlayerSetupRowView playerSetupRowView, int yCoordinate) {
        int xCoordinate = 1;
        addComponentToGridBagConstraints(playerSetupRowView.getPlayerLabel(), xCoordinate++, yCoordinate);
        addComponentToGridBagConstraints(playerSetupRowView.getPlayerNameTextField(), xCoordinate++, yCoordinate);
        addComponentToGridBagConstraints(playerSetupRowView.getIsComputerCheckBox(), xCoordinate++, yCoordinate);
        addComponentToGridBagConstraints(playerSetupRowView.getColorComboBox(), xCoordinate++, yCoordinate);
    }

    public void addActionListenerNrOfPlayersComboBox(ActionListener actionListener) {
        nrOfPlayersComboBox.addActionListener(actionListener);
    }

    public void addActionListenerStartGameButton(ActionListener actionListener) {
        startGame.addActionListener(actionListener);
    }

    public void addActionListenerGoToMainMenuButton(ActionListener actionListener) {
        goToMainMenu.addActionListener(actionListener);
    }

    public void setNrOfPlayersSelectedInComboBox(Integer nrOfPlayersSelected) {
        nrOfPlayersComboBox.setNrOfPlayersSelected(nrOfPlayersSelected);
    }

    public void setPlayerSetupRowViews(PlayerSetupRowView[] playerSetupRowViews) {
        this.playerSetupRowViews = playerSetupRowViews;
    }

    public void setEnableStartGameButton(boolean enable) {
        startGame.setEnabled(enable);
    }
}
