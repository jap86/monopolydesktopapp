package mappers;

import util.Logger;

public class Mapper {
    private Logger logger;

    public Mapper() {
        logger = Logger.getInstance();
    }

    protected void log(String message) {
        logger.log(getClass().getSimpleName(), message);
    }
}
