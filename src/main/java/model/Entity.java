package model;

import util.Logger;

public class Entity {
    private Logger logger;

    public Entity() {
        logger = Logger.getInstance();
    }

    protected void log(String message) {
        logger.log(getClass().getSimpleName(), message);
    }
}
