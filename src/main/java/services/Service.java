package services;

import util.Logger;

public class Service {
    private Logger logger;

    public Service() {
        logger = Logger.getInstance();
    }

    protected void log(String message) {
        logger.log(getClass().getSimpleName(), message);
    }

    protected void logError(String errorMessage) {
        logger.logError(getClass().getSimpleName(), errorMessage);
    }
}
