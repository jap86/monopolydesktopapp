package util;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Logger {
    private DateTimeFormatter dateTimeFormatter;
    private static volatile Logger instance;

    private Logger() {
        dateTimeFormatter = DateTimeFormatter.ofPattern("dd-MM-yyyy kk:mm:ss.SSS");
    }

    public static synchronized Logger getInstance() {
        if (instance ==  null) {
            synchronized (Logger.class) {
                if (instance == null) {
                    instance = new Logger();
                }
            }
        }
        return instance;
    }

    public void log(String className, String message) {
        printLn(format(className, message));
    }

    public void logError(String className, String message) {
        printError(format(className, message));
    }

    private String format(String className, String message) {
        LocalDateTime localDateTime = LocalDateTime.now();
        String date = localDateTime.format(dateTimeFormatter);
        String suffix = "-";
        String suffix2 = ":";
        return String.format("%-25s%-3s%-30s%-3s%s", date, suffix, className, suffix2, message);
    }

    private void printLn(String string) {
        System.out.println(string);
    }

    private void printError(String string) {
        System.err.println(string);
    }
}
