package valueObjects;

public class BoardSpaceValueObject extends ValueObject{
    public String name;
    public Integer identifier;

    public String getName() {
        return name;
    }

    public Integer getIdentifier() {
        return identifier;
    }
}
