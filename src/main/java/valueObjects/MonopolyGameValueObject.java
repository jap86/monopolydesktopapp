package valueObjects;

import java.util.List;
import java.util.Map;

public class MonopolyGameValueObject extends ValueObject{
    public List<PlayerValueObject> players;
    public Map<String, Integer> playerPositions;
    public String activePlayer;
    public String diceThrow1;
    public String diceThrow2;
    public Boolean canThrowAgain;
    public Integer nrOfDiceThrowsActivePlayer;

    public List<PlayerValueObject> getPlayers() {
        return players;
    }

    public Map<String, Integer> getPlayerPositions() {
        return playerPositions;
    }

    public String getActivePlayer() {
        return activePlayer;
    }

    public String getDiceThrow1() {
        return diceThrow1;
    }

    public String getDiceThrow2() {
        return diceThrow2;
    }

    public Boolean getCanThrowAgain() {
        return canThrowAgain;
    }

    public Integer getNrOfDiceThrowsActivePlayer() {
        return nrOfDiceThrowsActivePlayer;
    }
}
