package valueObjects;

public class PlayerValueObject extends ValueObject{
    public String name;
    public String playerColor;
    public Boolean isComputer;

    public String getName() {
        return name;
    }

    public String getPlayerColor() {
        return playerColor;
    }

    public Boolean getComputer() {
        return isComputer;
    }
}
