package valueObjects;

import util.Logger;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

public class ValueObject {
    private Logger logger = Logger.getInstance();
    private Class valueObject = this.getClass();
    private String valueObjectName = this.getClass().getSimpleName();
    private String prefix;
    private String suffix;
    private String delimiter;

    public ValueObject() {
        prefix = "{" + valueObjectName + ": ";
        suffix = "}";
        delimiter = ", ";
    }

    @Override
    public String toString() {
        List<String> values = getAllValues();
        StringJoiner stringJoiner = new StringJoiner(delimiter, prefix, suffix);
        for (String value : values) {
            stringJoiner.add(value);
        }
        return stringJoiner.toString();
    }

    private List<String> getAllValues() {
        List<String> values = new ArrayList<>();
        Field[] fields = valueObject.getDeclaredFields();
        for (Field field : fields) {
            String name = field.getName();
            String value = getValueOfField(name);
            values.add("[" + name + " = " + value + "]");
        }
        return values;
    }

    private String getValueOfField(String fieldName) {
        for (Method method : this.getClass().getMethods()) {
            String methodName = method.getName();
            if (methodName.startsWith("get")
                    && methodName.length() == (fieldName.length() + 3)
                    && methodName.toLowerCase().endsWith(fieldName.toLowerCase())) {
                Object returnObject = null;
                try {
                    returnObject = method.invoke(this);
                }
                catch (IllegalAccessException e) {
                    logError("Could not determine method " + methodName);
                }
                catch (InvocationTargetException e) {
                    logError("Could not determine method " + methodName);
                }
                return returnObject == null ? "null" : returnObject.toString();
            }
        }
        return "Value unknown";
    }

    private void logError(String message) {
        logger.logError(this.getClass().getSimpleName(), message);
    }
}
